package server.model

import javax.persistence.*


@Entity
@Table(name = "bes_lance_data")
class BesLanceData {
    constructor(data: String) {
        this.data = data
    }

    // JSON object? http://www.vivekpatidar.com/?p=13
    @Id
    @SequenceGenerator(name = "bes_lance_data_id_seq",
            sequenceName = "bes_lance_data_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO,
            generator = "bes_lance_data_id_seq")
    var id: Int? = null

    @Column(name = "data")
    var data: String

}