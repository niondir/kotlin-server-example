import org.flywaydb.core.Flyway
import org.hibernate.cfg.Configuration
import server.model.BesLanceData
import spark.Spark


fun main(args: Array<String>) {
    val flyway = Flyway()
    flyway.setDataSource("jdbc:postgresql://localhost:5432/postgres?sslmode=disable", "postgres", "admin")
    flyway.migrate()


    val cfg = Configuration()
            //.addClass(server.model.BesLanceData::class.java)
            .setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQL94Dialect")
            .setProperty("hibernate.connection.url", "jdbc:postgresql://localhost:5432/postgres?sslmode=disable")
            .setProperty("hibernate.connection.username", "postgres")
            .setProperty("hibernate.connection.password", "admin")
            .setProperty("show_sql", "true")
            .addAnnotatedClass(server.model.BesLanceData::class.java)
    //.setProperty("hibernate.connection.datasource", "java:comp/env/jdbc/test")
    // .setProperty("hibernate.order_updates", "true")


    val sessionFactory = cfg.buildSessionFactory()
    val entityManager = sessionFactory.createEntityManager()
    entityManager.getTransaction().begin()
    entityManager.persist(BesLanceData("""[{"B": 10}]"""))
    entityManager.getTransaction().commit()
    entityManager.close()

    Spark.port(8080)
    Spark.get("/hello/:name") { request, response -> "Hello: " + request.params(":name") }


}

fun setUp() {

}